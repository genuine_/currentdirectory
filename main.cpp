//
//  main.cpp
//  FileInputVector
//
//  Created by Gregory Lindor on 4/1/13.
//  Copyright (c) 2013 Gregory Lindor. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iterator>
#include <unistd.h>
#include <sys/param.h>
#include <mach-o/dyld.h>


const char* getExePath(char* real_path)
{
    //char real_path[MAXPATHLEN];
    char exe_path[MAXPATHLEN];
    
    uint32_t size = sizeof(exe_path);
    
    //Platform specific code goes here!!!!
    if( _NSGetExecutablePath(exe_path, &size) != 0)
    {
        std::cerr << "Error Occured with _NSGetExecutablePath " << std::endl;
    }
    else
    {
        //now attempt to get a real path (no symlinks)
        realpath(real_path, exe_path);
        std::cout << "The real path is: " << real_path << std::endl;
        return real_path;
    }
    //// !!!!!!!
    
    return nullptr;
    
}

int main(int argc, const char * argv[])
{
    
    char link_path[MAXPATHLEN];
    
    //Current Directory Stuff
    char buf[MAXPATHLEN];
    uint32_t size = sizeof(buf);
    if(_NSGetExecutablePath(buf,&size) == 0)
    {
        realpath(buf, link_path);
        std::cout << "The path is: " << link_path << std::endl;
    }
    else{
        std::cout << "Could not fix the path stuff, I suck.." << std::endl;
    }
    
    // strip filename from path
    std::string long_path(link_path);
    size_t pos = long_path.find_last_of("/\\");
    std::string relative_path =  long_path.substr(0,pos);
    
    std::cout << "working path: " << relative_path << std::endl;
    //set the current working path
    chdir(relative_path.c_str());
    
    std::ifstream in("testfile.txt");
    std::vector<std::string> vec;
    
    if(in){
        std::string s;
        while(std::getline(in, s))
        {
            vec.push_back(s);
            //print each line to output stream
            std::ostream_iterator<std::string> out(std::cout,"\n");
            std::copy( vec.begin(), vec.end(), out );
        }
    }
    else{
        std::cout << "file open error" << std::endl;
    }
    
    return 0;
}

